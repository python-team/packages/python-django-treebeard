python-django-treebeard (4.7.1-1) unstable; urgency=low

  * New upstream version 4.7.1
  * Refresh patches.
  * Use dh-sequence-python3.
  * Update year in d/copyright.

 -- Michael Fladischer <fladi@debian.org>  Fri, 23 Feb 2024 15:03:48 +0000

python-django-treebeard (4.7-1) unstable; urgency=medium

  * New upstream version 4.7
  * Build using pybuild-plugin-pyproject.

 -- Michael Fladischer <fladi@debian.org>  Mon, 19 Jun 2023 13:17:02 +0000

python-django-treebeard (4.6.1-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 4.6.2.

 -- Michael Fladischer <fladi@debian.org>  Thu, 09 Feb 2023 14:02:17 +0000

python-django-treebeard (4.6.0-1) unstable; urgency=low

  * New upstream release.
  * Drop jquery and Django 4 compatibility patches, fixed by upstream.
  * Remove -Files-Excluded for minified jquery, no longer included by
    upstream.
  * Update d/copyright with new years.
  * Update lintian source override for very-long-line-length-in-source-
    file.
  * Stop creating symlinks to packaged jquery, no longer used by
    upstream.
  * Fix permissions on .po files.
  * Refresh patches.

 -- Michael Fladischer <fladi@debian.org>  Fri, 06 Jan 2023 17:40:42 +0000

python-django-treebeard (4.5.1+dfsg-3) unstable; urgency=medium

  * Add patch for Django 4 compatibility (Closes: #1013521).
  * Bump Standards-Version to 4.6.1.0.
  * Update lintian-overrides.

 -- Michael Fladischer <fladi@debian.org>  Sun, 31 Jul 2022 16:38:20 +0000

python-django-treebeard (4.5.1+dfsg-2) unstable; urgency=low

  * Remove unnecessary autopkgtest-pkg-python testsuite.
  * Depend on python3-all for autopkgtests.
  * Update year in d/copyright.
  * Run wrap-and-sort -bast to reduce diff size of future changes.
  * Update lintian-overrides.

 -- Michael Fladischer <fladi@debian.org>  Fri, 14 Jan 2022 14:22:05 +0000

python-django-treebeard (4.5.1+dfsg-1) unstable; urgency=low

  [ Debian Janitor ]
  * Update renamed lintian tag names in lintian overrides.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Apply multi-arch hints.
    + python-django-treebeard-doc: Add Multi-Arch: foreign.

  [ Michael Fladischer ]
  * New upstream release.
  * Refresh patches.
  * Remove docs/_ext from Files-Excluded in d/copyright, no longer
    applies.
  * Bump minimum Django version to 2.2.
  * Bump debhelper version to 13.
  * Bump Standards-Version to 4.6.0.
  * Use uscan version 4.
  * Remove abandoned d/python-django-treebeard.docs.
  * Add python3-pytest-django to Build-Depends, required by tests.
  * Change README.rst to README.md.
  * Enable upstream testsuite for autopkgtests.
  * Update lintian overrides.

 -- Michael Fladischer <fladi@debian.org>  Fri, 10 Sep 2021 20:45:07 +0000

python-django-treebeard (4.3.1+dfsg-1) unstable; urgency=low

  * New upstream release.
  * Remove python-all from Build-Depends, Python2 is no longer used (Closes:
    #937709).
  * Set new Homepage URL.
  * Clean up d/rules to use dh where possible.
  * Clean up pytest artifacts to allow two builds in a row.
  * Bump debhelper version to 12.
  * Add patch to fix tests with Django 2.1.
  * Bump Standards-Version to 4.5.0.
  * Set Rules-Requires-Root: no.
  * Add ${sphinxdoc:Depends} to documentation package.
  * Install jQuery symlink in correct location.

 -- Michael Fladischer <fladi@debian.org>  Fri, 21 Feb 2020 22:03:58 +0100

python-django-treebeard (4.3+dfsg-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * Team upload.
  * Removed Python 2 support.

 -- Thomas Goirand <zigo@debian.org>  Sat, 27 Jul 2019 04:18:44 +0200

python-django-treebeard (4.3+dfsg-1) unstable; urgency=low

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field

  [ Michael Fladischer ]
  * New upstream release.
  * Refresh patches.
  * Bump Standards-Version to 4.1.4.
  * Fix spelling error in patch description.

 -- Michael Fladischer <fladi@debian.org>  Fri, 01 Jun 2018 14:56:47 +0200

python-django-treebeard (4.2.1+dfsg-1) unstable; urgency=low

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Michael Fladischer ]
  * New upstream release.
  * Refresh patches.

 -- Michael Fladischer <fladi@debian.org>  Thu, 29 Mar 2018 10:44:29 +0200

python-django-treebeard (4.2.0+dfsg-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches after git-dpm to gbp pq conversion
  * Always use pristine-tar.
  * Refresh patches, drop Re-enable-model-migrations-during-tests as it
    was applied upstream.
  * Clean up django_treebeard.egg-info/PKG-INFO to allow two builds in a
    row.
  * Bump debhelper compatibility and version to 11.
  * Bump Standards-Version to 4.1.3.
  * Run wrap-and-sort -bast to reduce diff size of future changes.
  * Enable autopkgtest-pkg-python testsuite.
  * Build documentation in override_dh_sphinxdoc.

 -- Michael Fladischer <fladi@debian.org>  Sun, 21 Jan 2018 15:56:59 +0100

python-django-treebeard (4.1.2+dfsg-1) unstable; urgency=low

  * New upstream release (Closes: #865934).
  * Bump Standards-Version to 4.0.0.
  * Add patch to re-enable model migrations during tests.

 -- Michael Fladischer <fladi@debian.org>  Wed, 12 Jul 2017 22:20:22 +0200

python-django-treebeard (4.1.0+dfsg-1) unstable; urgency=low

  * New upstream release.
  * Use python3-sphinx to build documentation.
  * Rebuild all django.mo and djangojs.mo files from source using
    python3-babel.
  * Drop Django 1.10 compatibility patches since they have been merged
    upstream.

 -- Michael Fladischer <fladi@debian.org>  Mon, 28 Nov 2016 20:00:59 +0100

python-django-treebeard (4.0.1+dfsg-2) unstable; urgency=medium

  * Replace django.conf.urls.patterns with lists (Closes: #828674).
  * Fix typo in long description.
  * Add patches to support Django 1.10:
    - Replace django.conf.urls.patterns with lists.
    - Pass through additional kwargs to support new `use_required_attribute`
      ModelForm argument.
    - Do not load templatetag  `cycle` from `future`.
    - Search request context processor in settings.TEMPLATES.
    - Add basic settings.TEMPLATES for tests.
  * Use https:// for copyright-format 1.0 URL.

 -- Michael Fladischer <fladi@debian.org>  Wed, 19 Oct 2016 11:29:57 +0200

python-django-treebeard (4.0.1+dfsg-1) unstable; urgency=low

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Michael Fladischer ]
  * New upstream release.
  * Refresh patches.
  * Bump Standards-Version to 3.9.8.
  * Update lintian overrides for long line in javascript source.

 -- Michael Fladischer <fladi@debian.org>  Thu, 05 May 2016 14:44:20 +0200

python-django-treebeard (4.0+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Add patch to initialize django in the sphinx configuration when building
    the documentation (Closes: #808513).
  * Add patch to use local opjects.inv files for intersphinx mappings.
  * Add patch to remove djangodocs extension from sphinx configuration.
  * Drop patch to remove unneeded pytest idmaker hack. It was merged
    upstream.
  * Refresh patch to remove version information from the jquery-ui
    filename.
  * Exclude djangodocs sphinx extension with unknown license.
  * Remove unneeded pytest idmaker hack. Using a supportd ids=arg instead.
  * Remove version infromation from jquery-ui filename
  * Exclude djangodocs sphinx extension with unknown license.
  * Add lintian overrides for false positives on non-minified javascript file.
  * Call sphinx-build directly to build documentation.
  * Clean .cache/v/cache/lastfailed to allow two builds in a row.

 -- Michael Fladischer <fladi@debian.org>  Sat, 02 Jan 2016 19:21:45 +0100

python-django-treebeard (3.0+dfsg-1) unstable; urgency=medium

  * Imported Upstream version 3.0+dfsg
  * Adopt package by setting DPMT as maintainer and myself as uploader (Closes: #738300).
  * Clean django_treebeard.egg-info/SOURCES.txt to allow two builds in a row.
  * Use pypi.debian.net service for uscan.
  * Make uscan aware of our DFSG repack in d/watch.
  * Use format 1.0 header in d/copyright.
  * Exclude minified jquery-ui file from source tarball.
  * Bump Standard-Version to 3.9.6.
  * Bump debhelper compatibility and version to 9.
  * Drop versioned dependency on python-django.
  * Rename Apache license to conform to copyright format 1.0.
  * Change packaging license to Apache as used by upstream. Thanks to Chris
    Lamb for giving me permission to do so.
  * Reformat d/control with cme and wrap-and-sort for better readability.
  * Add dh-python to Build-Depends-Indep and python-all to Build-Depends.
  * Switch buildsystem to pybuild.
  * Add python-pytest to Build-Depends-Indep.
  * Add doc-base registration for documentation.
  * Use sphinxdoc debhelper to set symlinks for documentation assets.
  * Create symlink to minified jQuery-UI file from package libjs-jquery-ui.
  * Add pytest_idmaker.patch taken from upstream to fix tests.
  * Add unversion_jquery-ui.patch to remove version information from the
    referenced jquery-ui file.
  * Add libjs-jquery-ui to Depends.
  * Drop d/pyversions and replace it with X-Python-Version.
  * Add support for Python3 with a separate binary package.
  * Install README.rst as documenation for both binary packages.
  * Move packaging repository to DPMT git.

 -- Michael Fladischer <fladi@debian.org>  Tue, 08 Sep 2015 18:42:45 +0200

python-django-treebeard (2.0~beta1-4) unstable; urgency=medium

  * QA upload.
  * Build using dh_python2

 -- Matthias Klose <doko@debian.org>  Sun, 13 Jul 2014 16:04:58 +0000

python-django-treebeard (2.0~beta1-3) unstable; urgency=medium

  * Orphaning package.

 -- Chris Lamb <lamby@debian.org>  Sun, 09 Feb 2014 00:08:33 +0000

python-django-treebeard (2.0~beta1-2) unstable; urgency=low

  * Add missing build dependency on python-setuptools. (Closes: #712302)

 -- Chris Lamb <lamby@debian.org>  Mon, 17 Jun 2013 08:05:29 +0100

python-django-treebeard (2.0~beta1-1) unstable; urgency=low

  * New upstream release (Closes: #711278)
    - Drop installation of examples.
  * Bump Standards-Version to 3.9.4.

 -- Chris Lamb <lamby@debian.org>  Mon, 10 Jun 2013 23:35:36 +0100

python-django-treebeard (1.61-3) unstable; urgency=low

  * Don't compress objects.inv as this breaks sphinx.ext.intersphinx.
    (Closes: #608768)

 -- Chris Lamb <lamby@debian.org>  Sun, 27 Feb 2011 21:29:46 +0000

python-django-treebeard (1.61-2) unstable; urgency=low

  * Update Vcs-{Git,Browser}.
  * Add python-django and graphviz to Build-Depends-Indep to ensure diagrams
    are generated on rebuild. (Closes: #608917)

 -- Chris Lamb <lamby@debian.org>  Sun, 27 Feb 2011 21:23:36 +0000

python-django-treebeard (1.61-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 3.9.1.

 -- Chris Lamb <lamby@debian.org>  Wed, 28 Jul 2010 21:05:33 -0400

python-django-treebeard (1.60-4) unstable; urgency=low

  * Bump python compatibility to 2.5. Thanks to Stefano Rivera
    <stefano@rivera.za.net>. (Closes: #582045)

 -- Chris Lamb <lamby@debian.org>  Wed, 19 May 2010 23:35:26 +0100

python-django-treebeard (1.60-3) unstable; urgency=low

  * Install .py files directly using dh_auto_install (Closes: #579541)

 -- Chris Lamb <lamby@debian.org>  Wed, 28 Apr 2010 15:10:30 +0100

python-django-treebeard (1.60-2) unstable; urgency=low

  * New upstream URL.
  * Update debian/copyright.

 -- Chris Lamb <lamby@debian.org>  Wed, 21 Apr 2010 10:12:21 +0100

python-django-treebeard (1.60-1) unstable; urgency=low

  * New upstream releae.
  * Update debian/watch - Google Code not being updated anymore.
  * Update location of upstream docs.
  * Bump Standards-Version to 3.8.4.
  * Switch to dpkg-source 3.0 (quilt) format.
  * Move to minimal dh7 debian/rules.

 -- Chris Lamb <lamby@debian.org>  Mon, 19 Apr 2010 15:16:08 +0100

python-django-treebeard (1.1-2) unstable; urgency=low

  * Bump Standards-Version to 3.8.3.
  * Fix typo in package description. Thanks to Reuben Thomas <rrt@sc3d.org>
    (Closes: #563331)
  * Update git repository locations.

 -- Chris Lamb <lamby@debian.org>  Tue, 05 Jan 2010 15:59:44 +0000

python-django-treebeard (1.1-1) unstable; urgency=low

  * Initial release. (Closes: #508035)

 -- Chris Lamb <lamby@debian.org>  Sun, 07 Dec 2008 04:50:21 +0000
