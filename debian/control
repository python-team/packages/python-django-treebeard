Source: python-django-treebeard
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Michael Fladischer <fladi@debian.org>,
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 python3-all,
Build-Depends-Indep:
 dh-sequence-python3,
 graphviz,
 pybuild-plugin-pyproject,
 python3-babel,
 python3-django (>= 2.2~),
 python3-pytest,
 python3-pytest-django,
 python3-setuptools,
 python3-sphinx,
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-django-treebeard
Vcs-Git: https://salsa.debian.org/python-team/packages/python-django-treebeard.git
Homepage: https://github.com/django-treebeard/django-treebeard
Rules-Requires-Root: no

Package: python-django-treebeard-doc
Architecture: all
Section: doc
Depends:
 libjs-jquery,
 ${misc:Depends},
 ${sphinxdoc:Depends},
Multi-Arch: foreign
Description: Efficient implementations of tree data structures for Django (documentation)
 Django Treebeard is a library that implements efficient data structures for
 storing hierarchical data in a database using the Django web development
 framework.
 .
 It currently includes 3 different tree implementations: adjacency list,
 materialized path and nested sets. Each one has it's own strengths and
 weaknesses but share the same API, so it’s easy to switch between them.
 .
 This package contains the documentation in searchable HTML format.

Package: python3-django-treebeard
Architecture: all
Depends:
 libjs-jquery-ui,
 python3-django (>= 2.2~),
 ${misc:Depends},
 ${python3:Depends},
Description: Efficient implementations of tree data structures for Django (Python3 version)
 Django Treebeard is a library that implements efficient data structures for
 storing hierarchical data in a database using the Django web development
 framework.
 .
 It currently includes 3 different tree implementations: adjacency list,
 materialized path and nested sets. Each one has it's own strengths and
 weaknesses but share the same API, so it’s easy to switch between them.
 .
 This package contains the Python 3 version of the library.
